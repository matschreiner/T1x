#### Installation
To install, run:
```
$ git clone https://gitlab.com/matschreiner/Transition1x
$ cd Transition1x
$ pip install .
```
if you want to run the ase\_db.py example or generate the dataset from scratch, instead install dependencies by running

```
$ pip install '.[example]'
```

To download the hdf5 file to a given path (default is './data'), run:

```
$ python download_t1x.py {path}
```

The data will be downloaded to the current folder if no path is specified.

#### Usage
In python run

```
from transition1x import Dataloader

dataloader = Dataloader(path_to_h5_file)
for molecule in dataloader:
    energy = molecule["wB97x_6-31G(d).energy"]
    ...
```

The elements in the data loader each represent a single molecule. It is a dictionary that has the following keys available:
*    rxn:                               the name of the reaction that the molecule is coming from
*    formula:                           chemical formula for the molecule.
*    positions:                         list of x, y, z coordinates of all atoms in the molecule in Å.
*    atomic_numbers:                    list of atomic numbers ordered in the same way as positions.
*    wB97x_6-31G(d).energy:             total energy of molecule in eV.
*    wB97x_6-31G(d).atomization_energy: atomization energy of molecule in eV.
*    wB97x_6-31G(d).forces:             list of x, y, z forces on each atom in eV/Å - atoms are ordered in the same way as in positions.


It is possible to provide a datasplit key to the dataloader from 'train', 'val' or 'test' to only iterate through the training, validation or test data, respectively.

```
dataloader = t1x.Dataloader(path_to_h5_file, datasplit='test')
for molecule in dataloader:
    energy = molecule["wB97x_6-31G(d).energy"] # Molecule from test-data
    ...
```

Finally, it is possible to go through the reactant, transition state and product only by setting 'only_final' kwarg to True when instantiating the data loader.
In this case the data loader will return dictionaries where the configurations can be accessed under 'product', 'transition_state' or 'reactant'.




```
dataloader = t1x.Dataloader(path_to_h5_file, only_final=True)
for molecule in dataloader:
    ts_energy = molecule["transition_state"]["wB97x_6-31G(d).energy"]
    r_energy = molecule["reactant"]["wB97x_6-31G(d).energy"]
    activation_energy = ts_energy - r_energy
    ...
```



#### Examples

##### ase_db example
The ase\_db.py example we generate an ase.db database where each row has forces and atomization\_energy in the data-field. This database can be generated by running the exampe

```
$ python example/ase_db.py {path_hdf5_in} {path_db_out}
```

By default, the path to h5 and db files are data/transition1x.{db/h5}

##### simple example
The simple.py example loops through all configurations in the dataset and prints them as pretty dicts.
Afterwards it will loop through the dataset again but this time with the data loader that only returns reactants, transition_states and products.

```
$ python example/simple.py {path_h5}
```


#### Data generation
To generate the dataset from scratch:
Download and install ORCA here 'https://www.orcasoftware.de/tutorials_orca/'.
The original data can be fetched here 'https://zenodo.org/record/3715478#.YyxLJexBxqs'. Download the zipped directory 'wb97xd3.tar.gz' - this 'https://zenodo.org/record/3715478/files/wb97xd3.tar.gz?download=1' is a direct link to the file.

Unzip the data and run the NEB script on all:
```
$ python scripts/neb.py --output {output_path} --reactant {reactant.xyz} --product {product.xyz} --transition-state {transition-state.xyz} --output {path_to_output} --orcabinary {path_to_orca_binary}
```

It is also possible to specify
```
--neb_fmax      # fmax threshhold for NEB
--cineb_fmax    # fmax threshhold for CINEB
--steps         # max steps for the algorithm
```
and their values are set as described in the paper by default.

Running the above code on a reaction will produce the following files in the {output_path} directory.

```
{output_path}/
 ├─ fmaxs.json
 ├─ neb.db
 ├─ converged
 ├─ plots/
 │   ├─ mep.png
 │   ├─ transition-state.png
 │   ├─ reactant.png
 │   ├─ product.png
 ├─ xyz/
 │   ├─ transition-state.xyz
 │   ├─ reactant.xyz
 │   ├─ product.xyz
 ├─ orca/
     ├─ ...
```

* fmaxs.json - contains a list with the fmax's for every iteration of the algorithm. This is used to filter paths later.
* neb.db - is an ase.db that contains all configurations encountered while runnning. Be aware that product and reactant is unchanging but is still saved after each iteration.
* converged - this is an empty file that serves to inform whether the reaction converged within the the given steps
* plots - this directory contains plots of the MEP and reactant, product and transition state
* xyz - this directory contains xyz files of reactant, product and transition state
* orca - this directory is used by orca to perform its calculations. ORCA files for last iteration of the algorithm can be found here.

Compile a JSON list with paths to all output directories for all converged reactions and run

```
$ python scripts/combine_dbs.py --h5file {path_to_h5_output} --rxns {path_to_json_list}
```

This will generate the Transition1x.h5 file that has been released with the paper.
Please feel free to contact me if you have any questions regarding the dataset.
